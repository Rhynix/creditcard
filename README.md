# vue credit card

> Credit card made using vue js

## Clone the Repo:

> git clone https://Rhynix@bitbucket.org/Rhynix/creditcard.git

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

---

## Cards surported

The type of cards that are surpported in my sample code are of the:.

1. **37..**
2. **55..**
3. **41..**
4. **60..**
5. **36..**
6. **62..**
7. **61..**

---

## Card with different character length

I have implemented the different card number set where it follows the set of (**4 6 4**), the card starts with code:.

1. **36..**

---

## Other cards:

Any other card with a different codes will work, but with a gray background color to show that they are not supported.

---