export const allDataState = {
    cardsaccepted: [
        { id: 37, class: 'american'},
        { id: 55, class: 'master'},
        { id: 41, class: 'visa'},
        { id: 60, class: 'hipercard'},
        { id: 36, class: 'diners'},
        { id: 62, class: 'unionpay'},
        { id: 61, class: 'discover'},
    ],
    newCardDetails: [],
    cardtype: 'defalt',
    typedName: 'your name here',
    validity: '••/••',
    cardNumber: '•••• •••• •••• ••••',
    dinersFormart: '•••• •••••• ••••',
    max: 16,
}