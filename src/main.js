import Vue from 'vue'
import App from './components/App.vue'
import Vuex from 'vuex'
Vue.use(Vuex)

new Vue({
  el: 'app',
  components:{App}
})